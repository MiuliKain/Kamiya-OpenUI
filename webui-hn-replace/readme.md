## 简介
该修改截止到原仓库 commit `df0a1f83815c771246a7b1bca85d63feaefad8d1` 是可用的，如后续版本出现问题请回滚至该commit。
通过替换原项目 `modules/api` 目录下的两个文件以实现在API模式下为每次生成指定Hypernetwork。
## 使用
WebUI支持以API模式启动，添加启动参数 `--nowebui` 或者 修改 webui.py 文件第 160行 即可以该模式启动。
`/sdapi/v1/txt2img` 路径为text2img API，以POST方式接收JSON数据，示例如下
```json
{
    "prompt": "masterpiece,best quality,1 girl,loli, small breasts,animal ears, white flower hair ornament, cute face,shy, long hair, white hair, lying , wet, gold blue dress ,sea, illustration, dynamic angle, cinematic lighting, beautiful detailed water",
    "negative_prompt": "nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry,hands",
    "prompt_style": "None", 
    "prompt_style2": "None", 
    "steps": 28,
    "sampler_index":"Euler a",
    "restore_faces": false,
    "tiling": false, 
    "n_iter": 1, 
    "batch_size": 1, 
    "cfg_scale": 12, 
    "seed": -1, 
    "subseed": -1, 
    "subseed_strength": 0, 
    "seed_resize_from_h": 0, 
    "seed_resize_from_w": 0,      
    "seed_enable_extras": false, 
    "height": 640, 
    "width": 640, 
    "enable_hr": false, 
    "denoising_strength": 0.7, 
    "firstphase_width": 0, 
    "firstphase_height": 0,
    "hypernetwork":"anime_2"
}
```
API自带队列支持，二次开发无需队列模块。